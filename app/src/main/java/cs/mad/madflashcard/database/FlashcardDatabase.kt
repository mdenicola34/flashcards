package cs.mad.madflashcard.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import cs.mad.madflashcard.entities.Flashcard
import cs.mad.madflashcard.entities.FlashcardsDao

@Database(entities = [Flashcard::class], version = 1)
abstract class FlashcardDatabase: RoomDatabase() {
    abstract fun FlashcardsDao(): FlashcardsDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: FlashcardDatabase? = null

        fun getDatabase(context: Context): FlashcardDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        FlashcardDatabase::class.java,
                        "app_database"
                ).fallbackToDestructiveMigration().build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}

