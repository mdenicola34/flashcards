package cs.mad.madflashcard.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import cs.mad.madflashcard.entities.FlashcardSet
import cs.mad.madflashcard.entities.FlashcardSetDao

@Database(entities = [FlashcardSet::class], version = 1)
abstract class FlashcardSetDatabase: RoomDatabase() {
    abstract fun FlashcardSetDao(): FlashcardSetDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: FlashcardSetDatabase? = null

        fun getDatabase(context: Context): FlashcardSetDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        FlashcardSetDatabase::class.java,
                        "app_database"
                ).fallbackToDestructiveMigration().build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }

}