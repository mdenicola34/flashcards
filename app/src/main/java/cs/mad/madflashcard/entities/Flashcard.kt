package cs.mad.madflashcard.entities

import androidx.room.*


@Entity
data class Flashcard(@PrimaryKey(autoGenerate = true) var question: String?, var answer: String?)

@Dao
interface FlashcardsDao {
    fun getHardcodedFlashcards(): List<Flashcard> {
        return mutableListOf(
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1")
        )

    }

    @Insert
    fun insert(vararg flashcard: Flashcard)

    @Insert
    fun insert(flashcard: List<Flashcard>)

    @Update
    fun update(flashcard: FlashcardSet)

    @Delete
    fun delete(flashcard: FlashcardSet)


}

