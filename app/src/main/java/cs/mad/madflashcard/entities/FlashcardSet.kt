package cs.mad.madflashcard.entities

import androidx.room.*

@Entity
data class FlashcardSet(@PrimaryKey(autoGenerate = true) var title: String?, var id: Long? = null)

@Dao
interface FlashcardSetDao {


    fun getHardcodedFlashcardSets(): List<FlashcardSet> {
        return mutableListOf(
            FlashcardSet("Set 1"),
            FlashcardSet("Set 1"),
            FlashcardSet("Set 1"),
            FlashcardSet("Set 1"),
            FlashcardSet("Set 1"),
            FlashcardSet("Set 1"),
            FlashcardSet("Set 1"),
            FlashcardSet("Set 1"),
            FlashcardSet("Set 1"),
            FlashcardSet("Set 1")
            )


    }
    @Insert
    fun insert(vararg flashcardSet: FlashcardSet)

    @Insert
    fun insert(flashcardSet: List<FlashcardSet>)

    @Update
    fun update(flashcardSet: FlashcardSet)

    @Delete
    fun delete(flashcardSet: FlashcardSet)
}

