package cs.mad.madflashcard.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cs.mad.madflashcard.database.FlashcardDatabase
import cs.mad.madflashcard.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.madflashcard.entities.Flashcard
import cs.mad.madflashcard.entities.FlashcardSet


class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding
    private val FlashcardDao by lazy { FlashcardDatabase.getDatabase(applicationContext).FlashcardsDao() }

    @SuppressLint("SetTextI8n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setContentView(binding.root)


        binding.addFlashcardButton.setOnClickListener {
                FlashcardDao.insert(
                        Flashcard(
                                null,
                                null

                        )
                )
                binding.addFlashcardButton.setText(" ")

        }

        binding.deleteSetButton.setOnClickListener {
            FlashcardDao.delete(
                    FlashcardSet(
                            title = null
                    )
            )

        }


        binding.studySetButton.setOnClickListener{
            startActivity(Intent(this, StudySetActivity::class.java))
        }
    }


}