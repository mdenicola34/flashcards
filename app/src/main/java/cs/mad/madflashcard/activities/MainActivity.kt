package cs.mad.madflashcard.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cs.mad.madflashcard.adapters.FlashcardSetAdapter
import cs.mad.madflashcard.database.FlashcardSetDatabase
import cs.mad.madflashcard.databinding.ActivityMainBinding
import cs.mad.madflashcard.entities.FlashcardSet

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val FlashcardSetDao by lazy { FlashcardSetDatabase.getDatabase(applicationContext).FlashcardSetDao() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupRecycler()
        setupButtons()
    }

    private fun setupRecycler() {
        binding.flashcardSetList.adapter = FlashcardSetAdapter(listOf(), FlashcardSetDao)
    }

    private fun setupButtons() {

        binding.createSetButton.setOnClickListener {
                FlashcardSetDao.insert(
                        FlashcardSet(
                                null,
                                null
                        )
                )
                binding.createSetButton.setText("")

            }
        }
    }





