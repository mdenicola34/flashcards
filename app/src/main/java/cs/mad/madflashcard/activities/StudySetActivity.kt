package cs.mad.madflashcard.activities

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cs.mad.madflashcard.databinding.ActivityStudySetBinding
import cs.mad.madflashcard.entities.Flashcard


class StudySetActivity : AppCompatActivity(){

    private lateinit var binding: ActivityStudySetBinding

    @SuppressLint("SetTextI18n")
    class StudySetActivity : AppCompatActivity() {
        private lateinit var binding: ActivityStudySetBinding
        private val flashcards = getHardcodedFlashcards().toMutableList()
        private val missedCards = mutableListOf<Flashcard>()
        private val initialCount = flashcards.size
        private var completedCount = 0
        private val missedCount
            get() = missedCards.size
        private var correctCount = 0
        private var isCardFlipped = false

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            binding = ActivityStudySetBinding.inflate(layoutInflater)
            setContentView(binding.root)

            updateCard()
            updateCompletedText()
            setupButtons()
            binding.cardView.setOnClickListener {
                flipCard()
            }
        }

        private fun missCurrent() {
            val current = flashcards.removeFirst()
            missedCards.add(current)
            flashcards.add(current)
            updateMissedText()
            isCardFlipped = false
            updateCard()
        }

        private fun skipCurrent() {
            val current = flashcards.removeFirst()
            flashcards.add(current)
            isCardFlipped = false
            updateCard()
        }

        private fun markCorrectCurrent() {
            val current = flashcards.removeFirst()
            if (flashcards.size == 0) {
                finish()
            } else {
                completedCount += 1
                updateCompletedText()

                if (!missedCards.contains(current)) {
                    correctCount += 1
                    updateCorrectText()
                }

                isCardFlipped = false
                updateCard()
            }
        }

        private fun flipCard() {
            isCardFlipped = !isCardFlipped
            updateCard()
        }

        private fun updateCard() {
            if (isCardFlipped) {
                binding.flashcardSetTitle.text = flashcards[0].answer
            } else {
                binding.flashcardSetTitle.text = flashcards[0].question
            }
        }

        private fun updateCompletedText() {
            binding.completedCountText.text = "$completedCount / $initialCount"
        }

        private fun updateMissedText() {
            binding.missedCountText.text = "Missed: $missedCount"
        }

        private fun updateCorrectText() {
            binding.correctCountText.text = "Correct: $correctCount"
        }

        private fun setupButtons() {
            binding.exitButton.setOnClickListener { finish() }
            binding.missedButton.setOnClickListener { missCurrent() }
            binding.skipButton.setOnClickListener { skipCurrent() }
            binding.correctButton.setOnClickListener { markCorrectCurrent() }
        }
    }}