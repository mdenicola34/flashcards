package cs.mad.madflashcard.adapters

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import cs.mad.madflashcard.databinding.ItemFlashcardSetBinding
import cs.mad.madflashcard.entities.FlashcardSet
import cs.mad.madflashcard.entities.FlashcardSetDao
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FlashcardSetAdapter(private var dataset: List<FlashcardSet>, private val dao: FlashcardSetDao):
    RecyclerView.Adapter<FlashcardSetAdapter.ViewHolder>() {

    private val dataSet = mutableListOf<FlashcardSet>()

    init {
        this.dataSet.addAll(dataSet)
    }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(val binding: ItemFlashcardSetBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemFlashcardSetBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: FlashcardSetAdapter.ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.binding.flashcardSetTitle.text= item.title
        viewHolder.itemView.setOnClickListener {
            AlertDialog.Builder(it.context)
                .setTitle(item.title)
                .setNegativeButton("Delete") { _, _ ->
                    GlobalScope.launch {
                        dao.delete(item)

                    }
                }
                .setPositiveButton("Done") {_,_ -> }
                .setNeutralButton("Edit") {_, _ ->
                    showCustomDialog(viewHolder.itemView.context, item)
                }
                .create()
                .show()
        }
    }

    fun showCustomDialog(context: Context, item: FlashcardSet) {
        val title = EditText(context)
        val body = EditText(context)
        title.setText(item.title)
        AlertDialog.Builder(context)
            .setCustomTitle(title)
            .setView(body)
            .setPositiveButton("Done") {_,_ ->
                GlobalScope.launch {
                    item.title = title.text.toString()
                    dao.update(item)
                }
            }
            .create()
            .show()
    }


    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun addItem(it: FlashcardSet) {
        dataSet.add(it)
        notifyItemInserted(dataSet.lastIndex)
    }




}